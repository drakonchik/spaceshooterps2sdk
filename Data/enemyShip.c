
#include "enemyShip.h"

typedef struct
{
	GSTEXTURE texture;
	
	float posX;
	float posY;
	float width;
	float height;
	float velocityX;
	float velocityY;
	bool isAlive;

	EnemyBullet[2];
} EnemySpaceShip;

typedef struct
{
	GSTEXTURE texture;
	
	float posX;
	float posY;
	float width;
	float height;
	float velocityX;
	float velocityY;
	bool isAlive;

} EnemyBullet;


void updateEnemyShip(struct EnemySpaceShip, u64 colour)
{
		EnemySpaceShip.posX += EnemySpaceShip.velocityX;
		EnemySpaceShip.posY += EnemySpaceShip.velocityY;
		
		drawEnemyShip(EnemySpaceShip, colour);
}

void drawEnemyShip(struct EnemySpaceShip, u64 colour)
{
	gsKit_TexManager_bind(gsGlobal, &EnemySpaceShip.texture);
	gsKit_prim_sprite_texture(gsGlobal, &EnemySpaceShip.texture,
                            EnemySpaceShip.posX, EnemySpaceShip.posY, 
							(EnemySpaceShip.posX + EnemySpaceShip.width), 
							(EnemySpaceShip.posY + EnemySpaceShip.height),
                            gsGlobal->Width, gsGlobal->Height,
                            EnemySpaceShip.texture.Width, EnemySpaceShip.texture.Height,
                            1, colour);
}

