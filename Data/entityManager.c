#include "include/entityManager.h"
#include "include/soundEffects.h"
#include "include/musicManager.h"

#include <stdlib.h>

extern Controller PlaystationGamePad;

PlayerBullet bulletArray[20];
Asteroid asteroidArray[50];
Player player;

GSTEXTURE AsteroidTexture;
GSTEXTURE BulletTexture;
GSTEXTURE PlayerTexture;
GSTEXTURE ExplosionTexture;

u64 AstTexCol;
u64 BulTexCol;
u64 PlaTexCol;
u64 ExsTexCol;

int MAX_BULLET_COUNT = 20;
int currentBulletCount = 0;
int asteroidLaunchTimer = 0;
int currentAsteroidCount = 0;
int AsteroidRate = 30;

int BulletChannels = 0; // 1 2 and 3;

int ExplosionChannels = 4; // 5 6 and 7;

// Sound Effect Structs.
SE ExplosionSE;
SE Shoot;

// Background Music
BGM GameMusic;

extern char* root;

extern bool GameOverBoolean;

// Initialize
void InitializeEntities(GSGLOBAL* gsGlobal)
{
	char* tempRoot = "";
	strncpy(tempRoot, root, strlen(root));
	// Initialize Player Bullet Texture
	//textureSetup(&BulletTexture);
	gsKit_texture_png(gsGlobal, &BulletTexture,  strcat(tempRoot,"Graphics/Sprites/bullet.png"));
	BulTexCol = GS_SETREG_RGBAQ(0x80,0x80,0x80,0x00,0x00);
	
	// Initialize Asteroid Texture
	//textureSetup(&AsteroidTexture);
	strncpy(tempRoot, root, strlen(root));
	gsKit_texture_png(gsGlobal, &AsteroidTexture, strcat(tempRoot,"Graphics/Sprites/asteroid.png"));
	AstTexCol = GS_SETREG_RGBAQ(0x80,0x80,0x80,0x80,0x00);

	// Initialize Player Texture
	//textureSetup(&PlayerTexture);
	strncpy(tempRoot, root, strlen(root));
	gsKit_texture_png(gsGlobal, &PlayerTexture, strcat(tempRoot,"Graphics/Sprites/player.png"));
	PlaTexCol = GS_SETREG_RGBAQ(0x80,0x80,0x80,0x00,0x00);	
	
	// Initialize Player Texture
	//textureSetup(&PlayerTexture);
	strncpy(tempRoot, root, strlen(root));
	gsKit_texture_png(gsGlobal, &ExplosionTexture, strcat(tempRoot,"Graphics/Sprites/ExplosionSheet.png"));
	ExsTexCol = GS_SETREG_RGBAQ(0x80,0x80,0x80,0x00,0x00);	
	
	player.X = 320.0f;
	player.Y = 380.0f;
	player.Speed = 3.0f;
	player.Health = 30;
	player.Heat = 0;
	player.isAlive = true;
	
	// This part here handles the initialization of the sound Effects for the Entities.
	initFormat();
	strncpy(tempRoot, root, strlen(root));
	ExplosionSE.fileName = strcat(tempRoot,"Audio/SE/Explosion.adp");
	LoadSoundEffect(&ExplosionSE);
	
	strncpy(tempRoot, root, strlen(root));
	Shoot.fileName = strcat(tempRoot,"Audio/SE/Shoot.adp");
	LoadSoundEffect(&Shoot);
	
	
	
	// Music Initialization
	//initMusicFormat();
	strncpy(tempRoot, root, strlen(root));
	GameMusic.fileName = strcat(tempRoot,"Audio/BGM/CatInSpaceMono.wav");
	LoadMusic(&GameMusic);
}

// Update
void UpdateEntities(GSGLOBAL* gsGlobal)
{

	// This here freezes the game
	PlayMusic(&GameMusic);

	UpdateInput();
	asteroidHitCheck(gsGlobal);
	
	if(!player.isAlive)
	{
		GameOverBoolean = true;
		printf("GameOver Should Start");
	}
	
	if(asteroidLaunchTimer > AsteroidRate)
	{
		int x = rand();
		x = x % 600;
		SpawnAsteroid((float)x, -15.0f);
		asteroidLaunchTimer = 0;
	}
	asteroidLaunchTimer++;

	// This part here makes sure that player is in bounds of the screen
	if(player.X < 0.0f)
	{
		player.X = 0.0f;
	}
	
	if(player.X > 640.0f- PlayerTexture.Width)
	{
		player.X = 640.0f - PlayerTexture.Width;
	}
	
	if(player.Y < 0.0f + PlayerTexture.Height)
	{
		player.Y = PlayerTexture.Height;
	}
	
	if(player.Y > 512.0f - PlayerTexture.Height)
	{
		player.Y = 512.0f - PlayerTexture.Height;
	}

	
	if(player.shoot)
	{
		PlaySoundEffect(&Shoot, BulletChannels);
		
		// This part here ensures that each bullet shoot uses it's own sound channel which cycles from 0 to 3
		BulletChannels++;
		
		if(BulletChannels > 3)
		{
			BulletChannels = 0;
		}
		SpawnBullet(player.X + PlayerTexture.Width / 2, player.Y + PlayerTexture.Height / 2);
	}
	
	// This segment here updates the Asteroids movement, and removes them in case
	// the asteroid has reached end of the screen or has hit something.
	for(int i = 0; i < currentAsteroidCount; i++)
	{
		asteroidArray[i].Y+= asteroidArray[i].speed;
		
		if(asteroidArray[i].Y > 512.0f || !asteroidArray[i].isAlive)
		{
						for (int j = i; j < currentAsteroidCount; j++)  
			{  
			    asteroidArray[j] = asteroidArray[j+1]; 
			}  
			currentAsteroidCount--;
		}
	}
	
	// This segment updates the bullet position on the screen 
	for(int i = 0; i < currentBulletCount; i++)
	{
	
		
		bulletArray[i].Y-= bulletArray[i].speed;
		//printf("Bullet Position X = %.2f Y = %.2f \n", bulletArray[i].X, bulletArray[i].Y);
		if( bulletArray[i].Y < -10.0f || !bulletArray[i].isAlive)
		{		
			for (int j = i; j < currentBulletCount; j++)  
			{  
			    bulletArray[j] = bulletArray[j+1]; 
			}  
			currentBulletCount--;
		}
	}
}

// Draw
void DrawEntities(GSGLOBAL* gsGlobal, u64 colour)
{
	
	gsKit_prim_sprite_texture(gsGlobal, &PlayerTexture,	player.X,  // X1
						player.Y,  // Y2
						0.0f,  // U1
						0.0f,  // V1
						PlayerTexture.Width + player.X, // X2
						PlayerTexture.Height +  player.Y, // Y2
						PlayerTexture.Width, // U2
						PlayerTexture.Height, // V2
						2,
						PlaTexCol);

	
	// Loop for drawing Asteroids
	for(int i = 0; i < currentAsteroidCount; i++)
	{
		if(asteroidArray[i].toExplode == false)
		{
			gsKit_prim_sprite_texture(gsGlobal, &AsteroidTexture,	asteroidArray[i].X,  // X1
						asteroidArray[i].Y,  // Y2
						0.0f,  // U1
						0.0f,  // V1
						AsteroidTexture.Width + asteroidArray[i].X, // X2
						AsteroidTexture.Height +  asteroidArray[i].Y, // Y2
						AsteroidTexture.Width, // U2
						AsteroidTexture.Height, // V2
						2,
						AstTexCol);
		}			
		else
		{
			gsKit_prim_sprite_texture(gsGlobal, &ExplosionTexture,	asteroidArray[i].X,  // X1
								asteroidArray[i].Y,  // Y2
								32.0f * asteroidArray[i].ExplosionFrame,  // U1
								0.0f,  // V1
								64.0f + asteroidArray[i].X, // X2
								64.0f +  asteroidArray[i].Y, // Y2
								32.0f * asteroidArray[i].ExplosionFrame + 32.0f, // U2
								ExplosionTexture.Height, // V2
								2,
								ExsTexCol);
			asteroidArray[i].ExplosionFrame++;
			if(asteroidArray[i].ExplosionFrame > 9)
				asteroidArray[i].isAlive = false;	
		}
	}
	
	// Loop for drawing Bullets
	for(int i = 0; i < currentBulletCount; i++)
	{
		gsKit_prim_sprite_texture(gsGlobal, &BulletTexture,	bulletArray[i].X,  // X1
						bulletArray[i].Y,  // Y2
						0.0f,  // U1
						0.0f,  // V1
						BulletTexture.Width + bulletArray[i].X, // X2
						BulletTexture.Height +  bulletArray[i].Y, // Y2
						BulletTexture.Width, // U2
						BulletTexture.Height, // V2
						2,
						BulTexCol);
	}
}

// This part here handles the input for player Ship
void UpdateInput()
{
	if(PlaystationGamePad.UP_KEY_DOWN)
	{
		player.Y -= player.Speed;
	}
	
	if(PlaystationGamePad.DOWN_KEY_DOWN)
	{
		player.Y += player.Speed;
	}
			
	if(PlaystationGamePad.LEFT_KEY_DOWN)
	{
		player.X -= player.Speed;
	}
			
	if(PlaystationGamePad.RIGHT_KEY_DOWN)
	{
		player.X += player.Speed;
	}
			
	// if Button pressed shoot equals true
	player.shoot = PlaystationGamePad.BUTTON_X_KEY_TAP;
	
	
}

// Spawns Asteroid at X, Y coordinates, provided below
void SpawnAsteroid(float posX, float posY)
{
	asteroidArray[currentAsteroidCount].X = posX;
	asteroidArray[currentAsteroidCount].Y = posY;
	asteroidArray[currentAsteroidCount].speed = 3.0f;
	asteroidArray[currentAsteroidCount].isAlive = true;
	currentAsteroidCount++;
	asteroidArray[currentAsteroidCount].ExplosionFrame = 0;
}

// Spawns Bullet at X, Y coordinates, based on player position
void SpawnBullet(float posX, float posY)
{
	bulletArray[currentBulletCount].X = posX;
	bulletArray[currentBulletCount].Y = posY;
	bulletArray[currentBulletCount].speed = 8.0f;
	bulletArray[currentBulletCount].isAlive = true;
	currentBulletCount++;
}

void textureSetup(GSTEXTURE *texture)
{
    texture->Width = 0;                 // Must be set by loader
    texture->Height = 0;                // Must be set by loader
    texture->PSM = GS_PSM_CT24;         // Must be set by loader
    texture->ClutPSM = 0;               // Default, can be set by loader
    texture->TBW = 0;                   // gsKit internal value
    texture->Mem = NULL;                // Must be allocated by loader
    texture->Clut = NULL;               // Default, can be set by loader
    texture->Vram = 0;                  // VRAM allocation handled by texture manager
    texture->VramClut = 0;              // VRAM allocation handled by texture manager
    texture->Filter = GS_FILTER_LINEAR; // Default

    // Do not load the texture to VRAM directly, only load it to EE RAM
    texture->Delayed = 1;
}


// This function checks if asteroid has been hit by the player Bullet
void asteroidHitCheck(GSGLOBAL* gsGlobal)
{
	for(int j = 0; j < currentBulletCount; j++)
	{
		for(int i = 0; i < currentAsteroidCount; i++)
		{
			
			if(bulletArray[j].X > asteroidArray[i].X && bulletArray[j].X < asteroidArray[i].X + AsteroidTexture.Width)
			{
			    if((bulletArray[j].Y > asteroidArray[i].Y && bulletArray[j].Y < asteroidArray[i].Y + AsteroidTexture.Height)&& !asteroidArray[i].toExplode) 
			    {
				    	asteroidArray[i].toExplode = true;
				    	bulletArray[j].isAlive = false;
				    	PlaySoundEffect2(&ExplosionSE, ExplosionChannels);			
				    	// This is same as with bullet Sound Effect channels
				    	ExplosionChannels++;
				    	if(ExplosionChannels > 7)
				    	{
				    		ExplosionChannels = 4;
				    	}
			    }
			    else
			    {

			    }
			
			}
			else
			{

			}
		}
	
	}
	
	for(int i = 0; i < currentAsteroidCount; i++)
		{
			
			if(player.X > asteroidArray[i].X && player.X < asteroidArray[i].X + AsteroidTexture.Width)
			{
			    if(player.Y > asteroidArray[i].Y && player.Y < asteroidArray[i].Y + AsteroidTexture.Height)
			    {
			    		asteroidArray[i].isAlive = false;
			    		//player.Health -=1;
			    		player.isAlive = false;
			    		PlaystationGamePad.HAPTIC_MOTOR = true;
			    }
			    else
			    {
					PlaystationGamePad.HAPTIC_MOTOR = false;
			    }
			
			}
			else
			{

			}
		}
}


