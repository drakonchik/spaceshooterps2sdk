#include "include/gameScreen.h"
#include "include/menuScreen.h"
#include "include/overScreen.h"
#include "include/starfield.h"
#include "include/entityManager.h"


extern StateMachine GameMachineState;

bool GameOverBoolean;

void GameStart(GSGLOBAL* gsGlobal)
{
	//printf("Initialising Game State\n");
	InitializeStarfield(gsGlobal);
	
	// Initialize all Entities
	InitializeEntities(gsGlobal);
	
	GameOverBoolean = false;
}

void GameUpdate(GSGLOBAL* gsGlobal)
{
	//printf("GameState Updating....\n");
	ScrollBackground(gsGlobal);
	UpdateEntities(gsGlobal);
	
	if(GameOverBoolean)
	{
		StateMachineChange(&GameMachineState, &OverState, gsGlobal);
	}
}

void GameDraw(GSGLOBAL* gsGlobal, u64 colour)
{
	//printf("Drawing Game State\n");
	DrawBackground(gsGlobal, colour);
	DrawEntities(gsGlobal,colour);
	
}

void GameEnd(GSGLOBAL* gsGlobal)
{
	// Mandatory Cleanup of the VRAM when changing state
	gsKit_vram_clear(gsGlobal);
	//UnloadMusic(&GameMusic);
}

StateManager GameState =
{
	GameStart,
	GameUpdate,
	GameDraw,
	GameEnd
};
