#ifndef MENU_STATE
#define MENU_STATE

#include "stateManager.h"
//#include "musicManager.h"

typedef struct
{
	GSTEXTURE texture;
	
	float posX;
	float posY;

}TitleScreen;

typedef struct
{
	GSTEXTURE texture;
	
	float posX;
	float posY;

}MenuText;

void MenuStart(GSGLOBAL* gsGlobal);
void MenuUpdate(GSGLOBAL* gsGlobal);
void MenuDraw(GSGLOBAL* gsGlobal, u64 colour);
void MenuEnd(GSGLOBAL* gsGlobal);

extern StateManager MenuState;

#endif
